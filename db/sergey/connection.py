from peewee import PostgresqlDatabase

from utils import config


db = PostgresqlDatabase(
    database=config['db']['name'],
    user=config['db']['user'],
    password=config['db']['password'],
    host=config['db']['host'],
    port=config['db']['port']
)

