from functools import wraps

def close_connect_db(db):
    def decorator(f):
        @wraps(f)
        def new_f(*args, **kwargs):
            result = f(*args, **kwargs)
            if not db.is_closed():
                db.close()
            return result
        return new_f
    return decorator

def reuse_connect_db(db):
    def decorator(f):
        @wraps(f)
        def new_f(*args, **kwargs):
            db.connect(reuse_if_open=True)
            return f(*args, **kwargs)
        return new_f
    return decorator