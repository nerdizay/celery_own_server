from flask import Flask, make_response, request

from tasks.test_task import test_task

from utils import custom_logger_catch


app = Flask(__name__)


@app.route('/')
def index():
    return make_response('сервис запущен...', 200)


@app.route('/test')
@custom_logger_catch
def test_wrapper():
    
    number = request.args.get('number')
    if not number:
        return make_response(f'Не передали number', 400)
    
    id_task = test_task.delay(int(number))
    return make_response(f'Задание отправлено. ID задания - {id_task}', 200)


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=8888)