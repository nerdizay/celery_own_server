from datetime import datetime, timedelta
import time

from loguru import logger

import requests

import configparser

from functools import wraps


config = configparser.ConfigParser()
config.read('config.ini')


def get_hour_time_utc(time_ekb: str) -> tuple[int, int]:  #'hh:mm'
    time_for_task = datetime.strptime(time_ekb, '%H:%M') - timedelta(hours=5)
    return (time_for_task.hour, time_for_task.minute)


def debug_only(record):
    return record["level"].name == "DEBUG"

def info_only(record):
    return record["level"].name == "INFO"

def error_only(record):
    return record["level"].name == "ERROR"

logger.add("debug.log", filter=debug_only)
logger.add("info.log", filter=info_only)
logger.add("error.log", filter=error_only)


def custom_logger_catch(f):
    @wraps(f)
    def new_f(*args, **kwargs):
        try:
            result = f(*args, **kwargs)
        except (Exception, BaseException) as e:
            logger.error(f"{str(e)}")
            notify_about_error_to_telegram(str(e), "celery")
            raise e
        else:
            return result

    return new_f


TELEGRAM_URL = 'https://api.telegram.org/bot'
API_TOKEN = config['telegram']['api_token']
METHOD = '/sendMessage'
FULL_URL = TELEGRAM_URL + API_TOKEN + METHOD
CHAT_IDS = config['telegram']['chat_ids'].split(',')

def notify_about_error_to_telegram(error_message: str, service: str = None) -> None:
    if service:
        error_message = f"В сервисе {service} ошибка:" + error_message
    for chat_id in CHAT_IDS:
        response = requests.get(
            url=FULL_URL,
            params={
                "chat_id": chat_id,
                "disable_web_page_preview": True,
                "text": error_message
            }
        )
        if response.status_code != 200:
            logger.info(
                "Оповеститель в телеграм \n"
                f"Код статуса: {response.status_code}\n"
                f"Сообщение: {error_message}"
            )
        time.sleep(1)