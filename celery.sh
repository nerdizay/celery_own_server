#!/bin/bash
case "$1" in
start)
sudo systemctl start flask-celery
sudo systemctl start celery
sudo systemctl start celerybeat;;
stop)
sudo systemctl stop flask-celery
sudo systemctl stop celery
sudo systemctl stop celerybeat;;
restart)
sudo systemctl restart flask-celery
sudo systemctl restart celery
sudo systemctl restart celerybeat;;
esac
