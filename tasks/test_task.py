from tasks.utils import *

from db.crm import *

        
@app_celery.task
@custom_logger_catch
@reuse_connect_db(db)
@close_connect_db(db)
def test_task(number: int | None = None) -> int:
    if number == 5:
        raise Exception("Ошибка число не может быть равно 5-ти")
    if not number:
        return 0
    else:
        return number + 1
