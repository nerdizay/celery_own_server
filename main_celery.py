from celery.schedules import crontab
from celery_instance import app_celery

from utils import get_hour_time_utc

from tasks.test_task import test_task

@app_celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Каждые 10 секунд #name ничего не даёт вроде
    # sender.add_periodic_task(10.0, add.s(1, 2), name='add every 10')

    # Каждые 30 секунд
    # sender.add_periodic_task(30.0, add.s(2, 4), name='add every 30')

    # Каждые 30 секунд. В чем разница?? Для чего expires??
    # sender.add_periodic_task(30.0, add.s(4, 8), expires=10)
    
    # hour, minute = get_hour_time_utc('21:40')
    # #Каждую среду в ...
    # sender.add_periodic_task(
    #     crontab(hour=hour, minute=minute, day_of_week=3),
    #     add.s(10, 25),
    #     name='add-numbers',
    # )
    
    # hour, minute = get_hour_time_utc('22:57')
    # sender.add_periodic_task(
    #     crontab(
    #         hour=hour,
    #         minute=minute,
    #         day_of_week='*'
    #     ),
    #     test_task.s(15),
    #     name='test_task',
    # )
    pass



    


