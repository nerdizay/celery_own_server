from celery import Celery

from utils import config

redis_host = config['redis']['host']
redis_port = config['redis']['port']
redis_claster = config['redis']['claster']

app_celery = Celery(
    'app_celery',
    broker_url=f"redis://{redis_host}:{redis_port}/{redis_claster}",
    result_backend=f"redis://{redis_host}:{redis_port}/{redis_claster}",
    timezone="UTC",
    broker_connection_retry_on_startup=True
)